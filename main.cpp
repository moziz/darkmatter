//#define __CL_ENABLE_EXCEPTIONS

#include <stdio.h>
#include <math.h>
#include <iostream>
#include <sstream>
#include <list>
#include <vector>
#include <fstream>
#include <unistd.h>
#include <CL/cl.hpp>

#define DATA_SIZE (16)
//#define VERBOSE

const float PI = 3.1415927f;

char* readFile(std::string name) {
  std::ifstream kernelFileReader;
  kernelFileReader.open(name.c_str());
  
  kernelFileReader.seekg(0, kernelFileReader.end);
  int kernelCodeLength = kernelFileReader.tellg();
  kernelFileReader.seekg(0, kernelFileReader.beg);
  
  char* fileContent = new char[kernelCodeLength + 1]; // + 1 for terminating null
  
  if (kernelFileReader.is_open()) {
    kernelFileReader.read(fileContent, kernelCodeLength);
  } else {
    //std::cerr << "Opening file \"" << name << "\" for reading failed!";
    return NULL;
  }
  kernelFileReader.close();
  
  fileContent[kernelCodeLength] = '\0'; // terminate the string
  
  return fileContent;
}

std::vector<float>* parseDataset(std::string filename) {
  std::string path;
  path = "datasets/" + filename;
  std::cout << "Reading dataset at " << path << std::endl;
  
  std::ifstream file;
  file.open(path.c_str());
  
  int entryCount;
  file >> entryCount;
  #ifdef VERBOSE
  std::cout << "Galaxies in dataset: " << entryCount << std::endl;
  #endif
  int angleCount = 2 * entryCount;
  
  std::vector<float>* angles = new std::vector<float>();
  angles->reserve(angleCount);
  
  #ifdef VERBOSE
  std::cout << "Galxy angles:" << std::endl;
  #endif
  for (int i = 0; i < angleCount; i++) {
    float a;
    file >> a;
    //a = a * (1.0f / 60.0f * PI / 180.0f);
    a = (a / 60.0f) * (PI / 180.0f);
    #ifdef VERBOSE
    std::cout << (i % 2? "" : "[ ") << a << (i % 2? " ]\n" : ",\t");
    #endif
    angles->push_back(a);
  }
  #ifdef VERBOSE
  std::cout << std::endl;
  #endif
  file.close();
  
  std::cout << "Processing dataset completed: " << path << std::endl;
  
  return angles;
}

int main() {
  cl_int err;
  
  char workingDirectory[FILENAME_MAX];
  std::cout << "Current working directory: " << getcwd(workingDirectory, sizeof(workingDirectory)) << std::endl;
  
  // Setup platform
  std::vector<cl::Platform> clPlatforms;
  cl::Platform clPlatform;
  
  err = cl::Platform::get(&clPlatforms);
  if (err != CL_SUCCESS || clPlatforms.size() < 1) {
    std::cerr << "No platfroms found!" << std::endl;
    exit(1);
  }
  clPlatform = clPlatforms[0];
  std::cout << "CL platform name: " << clPlatform.getInfo<CL_PLATFORM_NAME>() << std::endl;
  
  // Setup device
  std::vector<cl::Device> clDevices;
  cl::Device clDevice;
  
  err = clPlatform.getDevices(CL_DEVICE_TYPE_ALL, &clDevices);
  if (err != CL_SUCCESS || clDevices.size() < 1) {
    std::cerr << "No devices found!" << std::endl;
    exit(1);
  }
  clDevice = clDevices[0];
  std::cout << "Device name: " << clDevice.getInfo<CL_DEVICE_NAME>() << std::endl;
  
  // Setup context
  cl::Context clContext(clDevice);
  
  // Setup program
  std::string clKernelSource = readFile("darkmatter-kernel.cl");
  cl::Program clProgram(clContext, clKernelSource, true, &err);
  if (err != CL_SUCCESS) {
    std::cerr << "Kernel source building failed! Error log:\n" << clProgram.getBuildInfo<CL_PROGRAM_BUILD_LOG>(clDevice) << std::endl;
    exit(1);
  }
      
  // Load data
  std::cout << "Parse datasets..." << std::endl;
  std::vector<float>* datasetReal = parseDataset("data_100k_arcmin.dat");
  std::vector<float>* datasetRandom = parseDataset("flat_100k_arcmin.dat");
  int galaxyCount = datasetReal->size() / 2; // Naughty!
  
  // Create histograms
  float histogramBucketSize = (0.25f / 180.0f) * PI;
  float maxAnlge = (64.0f / 180.0f) * PI;
  int histogramBucketCount = (int)(maxAnlge / histogramBucketSize);
  std::cout << "Histogram: maxAnlge = " << maxAnlge << "; buckets = " << histogramBucketCount << "\n";
  
  std::vector<int> histogramReal;
  histogramReal.resize(histogramBucketCount, 0);
  std::vector<int> histogramRandom;
  histogramRandom.resize(histogramBucketCount, 0);
  std::vector<int> histogramRealRandom;
  histogramRealRandom.resize(histogramBucketCount, 0);
  
  // Create memory buffers  
  cl::Buffer bufferReal(clContext, CL_MEM_READ_ONLY, sizeof(float) * datasetReal->size());
  cl::Buffer bufferRandom(clContext, CL_MEM_READ_ONLY, sizeof(float) * datasetRandom->size());
  
  cl::Buffer bufferHistReal(clContext, CL_MEM_READ_WRITE, sizeof(int) * histogramBucketCount);
  cl::Buffer bufferHistRandom(clContext, CL_MEM_READ_WRITE, sizeof(int) * histogramBucketCount);
  cl::Buffer bufferHistRealRandom(clContext, CL_MEM_READ_WRITE, sizeof(int) * histogramBucketCount);
    
  // Setup command queue and upload data
  std::cout << "Upload data buffers" << std::endl;
  cl::CommandQueue clCommandQueue(clContext, clDevice);
  clCommandQueue.enqueueWriteBuffer(bufferReal, CL_TRUE, 0, sizeof(float) * datasetReal->size(), datasetReal->data());
  clCommandQueue.enqueueWriteBuffer(bufferRandom, CL_TRUE, 0, sizeof(float) * datasetRandom->size(), datasetRandom->data());
  
  clCommandQueue.enqueueWriteBuffer(bufferHistReal, CL_TRUE, 0, sizeof(int) * histogramReal.size(), histogramReal.data());
  clCommandQueue.enqueueWriteBuffer(bufferHistRandom, CL_TRUE, 0, sizeof(int) * histogramRandom.size(), histogramRandom.data());
  clCommandQueue.enqueueWriteBuffer(bufferHistRealRandom, CL_TRUE, 0, sizeof(int) * histogramRealRandom.size(), histogramRealRandom.data());
  
  // Setup kernel
  std::cout << "Setup kernel" << std::endl;
  auto kernelFunctor = cl::make_kernel<cl::Buffer, cl::Buffer, cl::Buffer, int, float, float>(clProgram, "angleHistogram");
  
  // Execute
  std::cout << "Execute the kernel for real galaxies" << std::endl;
  kernelFunctor(
    cl::EnqueueArgs(clCommandQueue, cl::NDRange(galaxyCount, galaxyCount)),
    bufferReal,
    bufferReal,
    bufferHistReal,
    histogramBucketCount,
    0.0f,
    maxAnlge
  );
  
  std::cout << "Execute the kernel for random galaxies" << std::endl;
  kernelFunctor(
    cl::EnqueueArgs(clCommandQueue, cl::NDRange(galaxyCount, galaxyCount)),
    bufferRandom,
    bufferRandom,
    bufferHistRandom,
    histogramBucketCount,
    0.0f,
    maxAnlge
  );
  
  std::cout << "Execute the kernel for real and random galaxies" << std::endl;
  kernelFunctor(
    cl::EnqueueArgs(clCommandQueue, cl::NDRange(galaxyCount, galaxyCount)),
    bufferReal,
    bufferRandom,
    bufferHistRealRandom,
    histogramBucketCount,
    0.0f,
    maxAnlge
  );
  
  // Download results
  std::cout << "Read execution results" << std::endl;
  clCommandQueue.enqueueReadBuffer(bufferHistReal, CL_TRUE, 0, sizeof(int) * histogramBucketCount, histogramReal.data());
  clCommandQueue.enqueueReadBuffer(bufferHistRandom, CL_TRUE, 0, sizeof(int) * histogramBucketCount, histogramRandom.data());
  clCommandQueue.enqueueReadBuffer(bufferHistRealRandom, CL_TRUE, 0, sizeof(int) * histogramBucketCount, histogramRealRandom.data());
  
  // Output results
  std::cout << "Write results to disk" << std::endl;
  std::stringstream csvStream;
  csvStream << "real,random,realrandom" << std::endl;
  for(int i = 0; i < histogramBucketCount; i++) {
    csvStream << histogramReal[i] << "," << histogramRandom[i] << "," << histogramRealRandom[i] << std::endl;
  }
  std::ofstream csvFile;
  csvFile.open("results.csv");
  csvFile << csvStream.str();
  csvFile.close();
  
  // Exit
  std::cout << "Execution finished" << std::endl;
  exit(0);
}