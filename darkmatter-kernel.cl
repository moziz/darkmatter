#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable

#define PI 3.1415927f

__kernel void angleHistogram(
  __global float* galaxiesA,
  __global float* galaxiesB,
  __global int* hist,
  int bucketCount,
  float minAngle,
  float maxAngle)
{  
  int i = 2 * get_global_id(0);
  int j = 2 * get_global_id(1);
  
  if (galaxiesA == galaxiesB && j < i + 1)
    return;

  float2 galaxyA;
  galaxyA.x = galaxiesA[i];
  galaxyA.y = galaxiesA[i + 1];
  
  float2 galaxyB;
  galaxyB.x = galaxiesB[j];
  galaxyB.y = galaxiesB[j + 1];
  
  float angleAB = acos(sin(galaxyA.y) * sin(galaxyB.y) + cos(galaxyA.y) * cos(galaxyB.y) * cos(galaxyA.x - galaxyB.x));
  
  int bucket = -1;
  if (angleAB <= maxAngle) {
    bucket = (int)((angleAB / maxAngle) * ((float)bucketCount));
    atomic_inc(hist + bucket);
  }
  //printf("[%i, %i] :: galaxyA = [%f, %f]; galaxyB = [%f, %f]; angleAB = %f; bucket = %i\n", i, j, galaxyA.x, galaxyA.y, galaxyB.x, galaxyB.y, angleAB, bucket);
}
